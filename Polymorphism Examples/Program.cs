﻿using System;
using System.Diagnostics;

namespace Polymorphism_Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            ProcessVehicles();
            System.GC.Collect();
            Console.ReadKey();
        }

        static void ProcessVehicles()
        {
            Console.WriteLine("Declaring a Vehicle");
            //Vehicle myVehicle = new Vehicle();

            Console.WriteLine("Declaring a Car");
            Car myCar = new Car();

            Console.WriteLine("Declaring a Motorcycle");
            Motorcycle myMotorCycle = new Motorcycle();

            //Console.WriteLine("GetNumWheels() for Vehicle: " + myVehicle.GetNumWheels());
            Console.WriteLine("GetNumWheels() for Car: " + myCar.GetNumWheels());
            Console.WriteLine("GetNumWheels() for Motorcycle: " + myMotorCycle.GetNumWheels());

            // No explicit cast needed for upcasting
            Vehicle myCarAsVehicle = myCar; 
            Vehicle myBikeAsVehicle = myMotorCycle;
            
            Console.WriteLine("GetNumWheels() for myCarAsVehicle: " + myCarAsVehicle.GetNumWheels());
            Console.WriteLine("GetNumWheels() for myBikeAsVehicle: " + myBikeAsVehicle.GetNumWheels());

            // Two types of explicit cast for downcasting
            Car myCarDowncast = myCarAsVehicle as Car; // "as" cast changes it to null if the type is not correct. Always check for null after use.
            Motorcycle myBikeDowncast = (Motorcycle)myBikeAsVehicle; // Throws an exception if cast to wrong type. Use if this is desired behaviour.
            
            Console.WriteLine("GetNumWheels() for myCarDowncast: " + myCarDowncast.GetNumWheels());
            Console.WriteLine("GetNumWheels() for myBikeDowncast: " + myBikeDowncast.GetNumWheels());

            // Examples of wrong type downcasting
            Car myCarWrongDowncast = myBikeAsVehicle as Car; 
            Motorcycle myBikeWrongDowncast = myCarAsVehicle as Motorcycle;

            Debug.Assert(myCarWrongDowncast != null, "myBikeAsVehicle was not type Car!");
            Debug.Assert(myBikeWrongDowncast != null, "myCarAsVehicle was not type Motorcycle!");

            if (myCarWrongDowncast != null)
                Console.WriteLine("GetNumWheels() for myCarWrongDowncast: " + myCarWrongDowncast.GetNumWheels());
            if (myBikeWrongDowncast != null)
                Console.WriteLine("GetNumWheels() for myBikeWrongDowncast: " + myBikeWrongDowncast.GetNumWheels());


            Console.WriteLine("Destroying all Vehicles");
        }
    }
}
