﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism_Examples
{
    class Motorcycle : Vehicle
    {
        public Motorcycle()
        {
            Console.WriteLine("Motorcycle constructed");
        }
        ~Motorcycle()
        {
            Console.WriteLine("Motorcycle destructed");
        }
        public override int GetNumWheels()
        {
            return 2;
        }
    }
}
