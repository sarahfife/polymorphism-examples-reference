﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism_Examples
{
    class Car : Vehicle
    {
        public Car()
        {
            Console.WriteLine("Car constructed");
        }
        ~Car()
        {
            Console.WriteLine("Car destructed");
        }
        public override int GetNumWheels()
        {
            return 4;
        }
    }
}
