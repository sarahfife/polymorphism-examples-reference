﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism_Examples
{
    abstract class Vehicle
    {
        public Vehicle()
        {
            Console.WriteLine("Vehicle constructed");
        }

        ~Vehicle()
        {
            Console.WriteLine("Vehicle destructed");
        }

        public abstract int GetNumWheels();
    }
}
